﻿using static Common.Calculations;
using static System.Console;

namespace GitLinkTest
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Hi");
            double a = 4.2;
            double b = 3.0;
            int c = 10;
            int d = 15;
            int e = 9;

            double x = SubHalf(c);

            WriteLine(GCD(c, d));
            WriteLine(GCD(d, e));
            WriteLine(GCD(c, e));

            double added = AddHalf(a) + b;
            WriteLine(added);
            ReadLine();
        }
    }
}
